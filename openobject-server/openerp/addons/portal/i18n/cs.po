# Czech translation for openobject-addons
# Copyright (c) 2011 Rosetta Contributors and Canonical Ltd 2011
# This file is distributed under the same license as the openobject-addons package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: openobject-addons\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2012-12-21 17:05+0000\n"
"PO-Revision-Date: 2012-05-10 17:33+0000\n"
"Last-Translator: Jiří Hajda <robie@centrum.cz>\n"
"Language-Team: Czech <openerp-i18n-czech@lists.launchpad.net >\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2012-12-22 06:05+0000\n"
"X-Generator: Launchpad (build 16378)\n"
"X-Poedit-Language: Czech\n"

#. module: portal
#: view:portal.payment.acquirer:0
msgid "Mako"
msgstr ""

#. module: portal
#: code:addons/portal/wizard/share_wizard.py:50
#, python-format
msgid "Please select at least one user to share with"
msgstr "Prosíme vyberte nejméně jednoho uživatele, s kterým chcete sdílet"

#. module: portal
#: view:portal.wizard:0
msgid ""
"Select which contacts should belong to the portal in the list below.\n"
"                        The email address of each selected contact must be "
"valid and unique.\n"
"                        If necessary, you can fix any contact's email "
"address directly in the list."
msgstr ""

#. module: portal
#: model:mail.group,name:portal.company_jobs
msgid "Company Jobs"
msgstr ""

#. module: portal
#: view:portal.payment.acquirer:0
msgid "amount: the total amount to pay, as a float"
msgstr ""

#. module: portal
#: view:portal.wizard.user:0
msgid "Contacts"
msgstr ""

#. module: portal
#: view:share.wizard:0
#: field:share.wizard,group_ids:0
msgid "Existing groups"
msgstr "Existující skupiny"

#. module: portal
#: view:res.groups:0
msgid "Portal Groups"
msgstr ""

#. module: portal
#: field:portal.wizard,welcome_message:0
msgid "Invitation Message"
msgstr ""

#. module: portal
#: view:res.groups:0
msgid "Non-Portal Groups"
msgstr ""

#. module: portal
#: code:addons/portal/wizard/share_wizard.py:54
#, python-format
msgid "Please select at least one group to share with"
msgstr "Prosíme vyberte nejméně jednu skupinu, s kterou chcete sdílet"

#. module: portal
#: view:share.wizard:0
msgid "Details"
msgstr ""

#. module: portal
#: model:ir.ui.menu,name:portal.portal_orders
msgid "Quotations and Sales Orders"
msgstr ""

#. module: portal
#: view:portal.payment.acquirer:0
msgid "reference: the reference number of the document to pay"
msgstr ""

#. module: portal
#: help:portal.payment.acquirer,visible:0
msgid ""
"Make this payment acquirer available in portal forms (Customer invoices, "
"etc.)"
msgstr ""

#. module: portal
#: model:ir.model,name:portal.model_share_wizard
msgid "Share Wizard"
msgstr "Průvodce sdílením"

#. module: portal
#: field:portal.wizard.user,email:0
msgid "Email"
msgstr ""

#. module: portal
#: model:ir.actions.client,help:portal.action_news
msgid ""
"<p>\n"
"                    Youd don't have unread company's news.\n"
"                </p>\n"
"            "
msgstr ""

#. module: portal
#: code:addons/portal/wizard/portal_wizard.py:194
#, python-format
msgid ""
"You must have an email address in your User Preferences to send emails."
msgstr ""
"Musíte mít emailovou adresu ve vašich uživatelských předvolbách pro zasílání "
"emailů."

#. module: portal
#: model:ir.actions.client,name:portal.action_jobs
#: model:ir.ui.menu,name:portal.portal_jobs
msgid "Jobs"
msgstr ""

#. module: portal
#: field:portal.wizard,user_ids:0
msgid "Users"
msgstr "Uživatelé"

#. module: portal
#: code:addons/portal/acquirer.py:82
#, python-format
msgid "Pay safely online"
msgstr ""

#. module: portal
#: code:addons/portal/acquirer.py:77
#, python-format
msgid "No online payment acquirers configured"
msgstr ""

#. module: portal
#: view:portal.payment.acquirer:0
msgid ""
"kind: the kind of document on which the payment form is rendered (translated "
"to user language, e.g. \"Invoice\")"
msgstr ""

#. module: portal
#: help:portal.wizard,portal_id:0
msgid "The portal that users can be added in or removed from."
msgstr ""

#. module: portal
#: code:addons/portal/wizard/share_wizard.py:38
#, python-format
msgid "Users you already shared with"
msgstr ""

#. module: portal
#: model:ir.actions.client,help:portal.action_jobs
msgid ""
"<p>\n"
"                    Youd don't have unread job offers.\n"
"                </p>\n"
"            "
msgstr ""

#. module: portal
#: view:portal.payment.acquirer:0
msgid ""
", so it may use Mako expressions.\n"
"                                The Mako evaluation context provides:"
msgstr ""

#. module: portal
#: model:ir.ui.menu,name:portal.portal_menu
#: field:portal.wizard,portal_id:0
#: field:res.groups,is_portal:0
#: model:res.groups,name:portal.group_portal
msgid "Portal"
msgstr "Portál"

#. module: portal
#: code:addons/portal/wizard/portal_wizard.py:34
#, python-format
msgid "Your OpenERP account at %(company)s"
msgstr "Váš účet OpenERP v %(company)s"

#. module: portal
#: model:res.groups,name:portal.group_anonymous
msgid "Anonymous"
msgstr ""

#. module: portal
#: field:portal.wizard.user,in_portal:0
msgid "In Portal"
msgstr ""

#. module: portal
#: model:ir.actions.client,name:portal.action_news
#: model:ir.ui.menu,name:portal.portal_company_news
msgid "News"
msgstr ""

#. module: portal
#: model:ir.ui.menu,name:portal.portal_after_sales
msgid "After Sale Services"
msgstr ""

#. module: portal
#: model:res.groups,comment:portal.group_portal
msgid ""
"Portal members have specific access rights (such as record rules and "
"restricted menus).\n"
"                They usually do not belong to the usual OpenERP groups."
msgstr ""

#. module: portal
#: model:ir.actions.act_window,name:portal.action_acquirer_list
#: view:portal.payment.acquirer:0
msgid "Payment Acquirers"
msgstr ""

#. module: portal
#: model:ir.ui.menu,name:portal.portal_projects
msgid "Projects"
msgstr ""

#. module: portal
#: model:ir.actions.client,name:portal.action_mail_inbox_feeds_portal
#: model:ir.ui.menu,name:portal.portal_inbox
msgid "Inbox"
msgstr ""

#. module: portal
#: view:share.wizard:0
#: field:share.wizard,user_ids:0
msgid "Existing users"
msgstr "Existující uživatelé"

#. module: portal
#: field:portal.wizard.user,wizard_id:0
msgid "Wizard"
msgstr "Průvodce"

#. module: portal
#: field:portal.payment.acquirer,name:0
msgid "Name"
msgstr ""

#. module: portal
#: model:ir.model,name:portal.model_res_groups
msgid "Access Groups"
msgstr ""

#. module: portal
#: view:portal.payment.acquirer:0
msgid "uid: the current user id"
msgstr ""

#. module: portal
#: view:portal.payment.acquirer:0
msgid ""
"quote(): a method to quote special string character to make them suitable "
"for inclusion in a URL"
msgstr ""

#. module: portal
#: help:res.groups,is_portal:0
msgid "If checked, this group is usable as a portal."
msgstr ""

#. module: portal
#: field:portal.payment.acquirer,form_template:0
msgid "Payment form template (HTML)"
msgstr ""

#. module: portal
#: field:portal.wizard.user,partner_id:0
msgid "Contact"
msgstr ""

#. module: portal
#: model:ir.model,name:portal.model_mail_mail
msgid "Outgoing Mails"
msgstr ""

#. module: portal
#: code:addons/portal/wizard/portal_wizard.py:193
#, python-format
msgid "Email required"
msgstr "Požadován email"

#. module: portal
#: model:ir.ui.menu,name:portal.portal_messages
msgid "Messaging"
msgstr ""

#. module: portal
#: model:res.groups,comment:portal.group_anonymous
msgid ""
"Anonymous users have specific access rights (such as record rules and "
"restricted menus).\n"
"                They usually do not belong to the usual OpenERP groups."
msgstr ""

#. module: portal
#: model:ir.model,name:portal.model_portal_payment_acquirer
msgid "Online Payment Acquirer"
msgstr ""

#. module: portal
#: model:mail.group,name:portal.company_news_feed
msgid "Company News"
msgstr ""

#. module: portal
#: code:addons/portal/acquirer.py:76
#, python-format
msgid ""
"You can finish the configuration in the <a href=\"%s\">Bank&Cash settings</a>"
msgstr ""

#. module: portal
#: view:portal.payment.acquirer:0
msgid "cr: the current database cursor"
msgstr ""

#. module: portal
#: model:ir.actions.client,help:portal.action_mail_inbox_feeds_portal
msgid ""
"<p>\n"
"                    <b>Good Job!</b> Your inbox is empty.\n"
"                </p><p>\n"
"                    Your inbox contains private messages or emails sent to "
"you\n"
"                    as well as information related to documents or people "
"you\n"
"                    follow.\n"
"                </p>\n"
"            "
msgstr ""

#. module: portal
#: view:portal.payment.acquirer:0
msgid ""
"object: the document on which the payment form is rendered (usually an "
"invoice or sales order record)"
msgstr ""

#. module: portal
#: help:portal.wizard,welcome_message:0
msgid "This text is included in the email sent to new users of the portal."
msgstr ""

#. module: portal
#: model:ir.ui.menu,name:portal.portal_company
msgid "About Us"
msgstr ""

#. module: portal
#: view:portal.payment.acquirer:0
msgid ""
"currency: the currency record in which the document is issued (e.g. "
"currency.name could be EUR)"
msgstr ""

#. module: portal
#: view:portal.payment.acquirer:0
msgid "Payment Acquirer"
msgstr ""

#. module: portal
#: code:addons/portal/wizard/portal_wizard.py:35
#, python-format
msgid ""
"Dear %(name)s,\n"
"\n"
"You have been given access to %(portal)s.\n"
"\n"
"Your login account data is:\n"
"Database: %(db)s\n"
"Username: %(login)s\n"
"\n"
"In order to complete the signin process, click on the following url:\n"
"%(url)s\n"
"\n"
"%(welcome_message)s\n"
"\n"
"--\n"
"OpenERP - Open Source Business Applications\n"
"http://www.openerp.com\n"
msgstr ""

#. module: portal
#: view:portal.wizard:0
msgid "or"
msgstr ""

#. module: portal
#: model:portal.payment.acquirer,form_template:portal.paypal_acquirer
msgid ""
"\n"
"% if object.company_id.paypal_account:\n"
"<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\" "
"target=\"_blank\">\n"
"  <input type=\"hidden\" name=\"cmd\" value=\"_xclick\"/>\n"
"  <input type=\"hidden\" name=\"business\" "
"value=\"${object.company_id.paypal_account}\"/>\n"
"  <input type=\"hidden\" name=\"item_name\" "
"value=\"${object.company_id.name} ${kind.title()} ${reference}\"/>\n"
"  <input type=\"hidden\" name=\"amount\" value=\"${amount}\"/>\n"
"  <input type=\"hidden\" name=\"currency_code\" "
"value=\"${currency.name}\"/>\n"
"  <input type=\"image\" name=\"submit\" "
"src=\"https://www.paypal.com/en_US/i/btn/btn_paynowCC_LG.gif\"/>\n"
"</form>\n"
"% endif\n"
"            "
msgstr ""

#. module: portal
#: model:ir.model,name:portal.model_portal_wizard_user
msgid "Portal User Config"
msgstr "Nastavení uživatele portálu"

#. module: portal
#: view:portal.payment.acquirer:0
msgid ""
"If the template renders to an empty result in a certain context it will be "
"ignored, as if it was inactive."
msgstr ""

#. module: portal
#: field:portal.payment.acquirer,visible:0
msgid "Visible"
msgstr ""

#. module: portal
#: code:addons/portal/wizard/share_wizard.py:39
#, python-format
msgid "Existing Groups (e.g Portal Groups)"
msgstr ""

#. module: portal
#: view:portal.wizard:0
msgid "Cancel"
msgstr "Zrušit"

#. module: portal
#: view:portal.wizard:0
msgid "Apply"
msgstr ""

#. module: portal
#: view:portal.payment.acquirer:0
msgid "ctx: the current context dictionary"
msgstr ""

#. module: portal
#: view:portal.payment.acquirer:0
msgid ""
"This is an HTML form template to submit a payment through this acquirer.\n"
"                                The template will be rendered with"
msgstr ""

#. module: portal
#: code:addons/portal/mail_mail.py:42
#, python-format
msgid ""
"Access your personal documents through <a href=\"%s\">our Customer Portal</a>"
msgstr ""

#. module: portal
#: view:portal.payment.acquirer:0
msgid "Form Template"
msgstr ""

#. module: portal
#: model:ir.actions.act_window,name:portal.partner_wizard_action
#: model:ir.model,name:portal.model_portal_wizard
#: view:portal.wizard:0
msgid "Portal Access Management"
msgstr ""

#~ msgid "Group"
#~ msgstr "Skupina"

#~ msgid "Portal User"
#~ msgstr "Uživatel portálu"

#~ msgid "Enable this option to override the Menu Action of portal users"
#~ msgstr "Povolte tuto volbu k přepsání Akcí nabídky uživatelů portálu"

#~ msgid "E-mail"
#~ msgstr "E-mail"

#~ msgid "The chosen company is not in the allowed companies for this user"
#~ msgstr ""
#~ "Vybraná společnost není v povolených společnostech pro tohoto uživatele"

#~ msgid "Widgets"
#~ msgstr "Udělátka"

#~ msgid "Send Invitations"
#~ msgstr "Poslat pozvání"

#~ msgid "The url where portal users can connect to the server"
#~ msgstr "URL, kde uživatelé portálu se mohou připojit k serveru"

#~ msgid "Widget"
#~ msgstr "Udělátko"

#~ msgid "This text is included in the welcome email sent to the users"
#~ msgstr "Tento text je zahrnut v uvítacím emailu zaslatném uživatelům"

#~ msgid "If set, replaces the standard menu for the portal's users"
#~ msgstr ""
#~ "Pokud je nastaveno, nahrazuje standardní nabídku pro uživatele portálu"

#~ msgid "Parent Menu"
#~ msgstr "Nadřazené menu"

#~ msgid "Portal Name"
#~ msgstr "Jméno portálu"

#~ msgid "Portal Users"
#~ msgstr "Uživatelé portálu"

#~ msgid "Override Menu Action of Users"
#~ msgstr "Přepsat Akce nabídky pro uživatele"

#~ msgid "Menu Action"
#~ msgstr "Akce nabídky"

#~ msgid "User Name"
#~ msgstr "Uživatelské jméno"

#~ msgid "Portal Widgets"
#~ msgstr "Pomůcky portálu"

#, python-format
#~ msgid "%s Menu"
#~ msgstr "%s Nabídka"

#~ msgid "The portal in which new users must be added"
#~ msgstr "Portál, ve kterém musí být přidání noví uživatelé"

#~ msgid "Widgets assigned to portal users"
#~ msgstr "Pomůcky přiřazené k těmto uživatelům portálu"

#, python-format
#~ msgid "(missing url)"
#~ msgstr "(chybějící url)"

#~ msgid ""
#~ "Will be used as user login.  Also necessary to send the account information "
#~ "to new users"
#~ msgstr ""
#~ "Bude použito jako přihlašovací jméno uživatele. Také je zapotřebí zaslat "
#~ "informace účtu novým uživatelům"

#~ msgid "Language"
#~ msgstr "Jazyk"

#~ msgid "URL"
#~ msgstr "URL"

#~ msgid "Widgets assigned to Users"
#~ msgstr "Pomůcky přiřazené uživatelům"

#~ msgid "The language for the user's user interface"
#~ msgstr "Jazyk pro uživatelské rozhraní uživatelů"

#~ msgid "Website"
#~ msgstr "Webová stránka"

#~ msgid "Create Parent Menu"
#~ msgstr "Vytvořit nadřazenou nabídku"

#~ msgid ""
#~ "The following text will be included in the welcome email sent to users."
#~ msgstr ""
#~ "Následující text bude zahrnut v uvítacím emailu zalsatném uživatelům."

#~ msgid "res.users"
#~ msgstr "res.users"

#~ msgid "Invalid email address"
#~ msgstr "Neplatná emailová adresa"

#~ msgid "ir.ui.menu"
#~ msgstr "ir.ui.menu"

#~ msgid "Portals"
#~ msgstr "Portály"

#~ msgid "The menu action opens the submenus of this menu item"
#~ msgstr "Akce nabídky otevírající podnabídku této položky nabídky"

#~ msgid "Sequence"
#~ msgstr "Posloupnost"

#~ msgid "Related Partner"
#~ msgstr "Vztažený partner"

#~ msgid "Portal Menu"
#~ msgstr "Nabídka portálu"

#~ msgid "You can not have two users with the same login !"
#~ msgstr "Nemůžete mít dva uživatele se stejným přihlašovacím jménem !"

#~ msgid "Invitation message"
#~ msgstr "Zpráva pozvánky"

#, python-format
#~ msgid ""
#~ "Dear %(name)s,\n"
#~ "\n"
#~ "You have been created an OpenERP account at %(url)s.\n"
#~ "\n"
#~ "Your login account data is:\n"
#~ "Database: %(db)s\n"
#~ "User:     %(login)s\n"
#~ "Password: %(password)s\n"
#~ "\n"
#~ "%(message)s\n"
#~ "\n"
#~ "--\n"
#~ "OpenERP - Open Source Business Applications\n"
#~ "http://www.openerp.com\n"
#~ msgstr ""
#~ "Vážený %(name)s,\n"
#~ "\n"
#~ "Byl vám vytvořen OpenERP účet na %(url)s.\n"
#~ "\n"
#~ "Váše přihlašovací údaje účtu jsou:\n"
#~ "Databáze: %(db)s\n"
#~ "Uživatel:     %(login)s\n"
#~ "Heslo: %(password)s\n"
#~ "\n"
#~ "%(message)s\n"
#~ "\n"
#~ "--\n"
#~ "OpenERP - Open Source Business Applications\n"
#~ "http://www.openerp.com\n"

#~ msgid "Portal Wizard"
#~ msgstr "Průvodce portálu"

#~ msgid "The user's real name"
#~ msgstr "Skutečné jméno uživatele"

#~ msgid "Add Portal Access"
#~ msgstr "Přidat přístup portálu"

#~ msgid "Partner"
#~ msgstr "Partner"

#~ msgid ""
#~ "\n"
#~ "A portal helps defining specific views and rules for a group of users (the\n"
#~ "portal group).  A portal menu, widgets and specific groups may be assigned "
#~ "to\n"
#~ "the portal's users.\n"
#~ "            "
#~ msgstr ""
#~ "\n"
#~ "Portál pomáhá stanovit určité pohledy a pravidla pro skupiny uživatelů\n"
#~ "(skupinu protálu).  Nabídka portálu, pomůcky a určité skupiny mohou být "
#~ "přiřazeny k\n"
#~ "uživatelům portálu.\n"
#~ "            "

#~ msgid "Manager"
#~ msgstr "Vedoucí"

#~ msgid "The group corresponding to this portal"
#~ msgstr "Skupina odpovídající tomuto portálu"

#~ msgid ""
#~ "Portal managers have access to the portal definitions, and can easily "
#~ "configure the users, access rights and menus of portal users."
#~ msgstr ""
#~ "Vedoucí portálu mají přístup k definici portálu a mohou snadno nastavit "
#~ "uživatel, přístupová práva a nabídky uživatelů portálu."

#~ msgid "Portal officers can create new portal users with the portal wizard."
#~ msgstr ""
#~ "Úředníci portálu mohou vytvářet uživatele portálu pomocí průvodce portálu."

#~ msgid "Officer"
#~ msgstr "Úředník"
